/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package netaddr

import (
	"testing"
)

func TestNetAddrSetoidReturnsRelativeComplement(t *testing.T) {
	na0, _ := IP("192.0.2.0/25")
	na1, _ := IP("192.0.2.128/25")
	na2, _ := IP("192.0.2.0/24")
	na3, _ := IP("192.0.2.126/31")
	s0 := NewNetAddrSetoid([]*NetAddr{})
	s1 := s0.Insert(na0).Insert(na1)
	if !s1.Blocks()[0].IsEqual(na2) {
		t.Errorf("Insert: expected %s; got %s", na2, s1.Blocks()[0])
		return
	}
	s2 := NewNetAddrSetoid([]*NetAddr{na3})
	s3 := RelComp(s1, s2)
	s3b := NewNetAddrSetoid([]*NetAddr{
		&NetAddr{[]byte{192, 0, 2, 0}, uint8(26)},
		&NetAddr{[]byte{192, 0, 2, 64}, uint8(27)},
		&NetAddr{[]byte{192, 0, 2, 96}, uint8(28)},
		&NetAddr{[]byte{192, 0, 2, 112}, uint8(29)},
		&NetAddr{[]byte{192, 0, 2, 120}, uint8(30)},
		&NetAddr{[]byte{192, 0, 2, 124}, uint8(31)},
		&NetAddr{[]byte{192, 0, 2, 128}, uint8(25)},
	})
	if !s3.IsEqual(s3b) {
		t.Errorf("RelComp:\nexpected\n%s;\ngot\n%s", s3b, s3)
	}
	s4 := Union(s3, s2)
	if !s1.IsEqual(s4) {
		t.Errorf("Union: expected %+v; got %+v", s1, s4)
	}
}
